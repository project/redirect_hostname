
-- SUMMARY --

This Redirect Hostname module is based on the popular redirect module. It allows
for redirects based on source hostname, not just source path.

Examples:
www.productX.com => www.example.com/productX
www.productX.com/promo => www.example.com/productX/promo

For a full description of the module, visit the project page:
  http://www.drupal.org/sandbox/lucashedding/2242839

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/2242839


-- REQUIREMENTS --

* redirect


-- INSTALLATION --

* Install as usual, for further information see
  http://drupal.org/documentation/install/modules-themes/modules-7.


-- CONTACT --

Current maintainer:
* Lucas Hedding (heddn) - http://drupal.org/user/1463982
